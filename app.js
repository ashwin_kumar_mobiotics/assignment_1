var app=new Vue({
    el:"#main",
    data(){
        return {
            count:0,
            intervalid:null
        }
    },
    computed:{
        countText(){
            return  `Counter value is ${this.count}`
        }
    },
    watch:{
        count(){
            if(this.count>=20){
                clearInterval(this.intervalid);
                this.intervalid=null;
            }
        }
    },
    methods:{
        toggleUpdate(){
            if(this.intervalid){
                clearInterval(this.intervalid);
                this.intervalid=null;
            }
            else {
                if(this.count===20)this.count=0;
                this.intervalid=setInterval(()=>{this.count++},1000);
            }
                  
        }
    },
    mounted(){
        this.intervalid=setInterval(()=>{this.count++},1000);
    },
    template:`<div class="app"><count-output :intervalid="intervalid"  :countval="countText" @toggle-update="toggleUpdate"/></div>`,
    components:{'count-output':childComp(["countval","intervalid"],{setButtonText},
    `<div class="output">
            <div v-text="countval"></div>
         <button @click="$emit('toggle-update')">{{setButtonText}}</button>
    </div>`)}

})


function childComp(props,computed,template){
    return {
        props,
        computed,
        template
    }
}

function setButtonText(){
    if(this.intervalid!==null)return "stop";
    else return this.countval.includes("20")?"start":"continue";
}